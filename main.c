#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define STATE_TRANSITION_LAYERS 2 
#define STATE_TRANSITION_NEURONS 1000
#define STATE_TRANSITION_EXIT_NEURONS 500
#define POLICY_LAYERS 2 
#define POLICY_NEURONS 1000
#define POLICY_EXIT_NEURONS 50 
#define NUM_ACTIONS 5
#define NUM_ACTION_REPRESENTATIVES 10
#define STATE_SIZE 1000
#define SENSORS_SIZE 500 // must be = STATE_SIZE - STATE_TRANSITION_EXIT_NEURONS
#define RADIUS 10 // make this even 
#define ENVIRONMENT_SIZE 30
#define NUM_ENEMIES 10
#define MAX_REWARD 1.0
#define REWARD_HISTORY_SIZE 50
#define THRESHOLD 0.5

int POSITION[2];
int ACTION[2];
int **ENEMY_POSITION; // Dangerous objects
int **ACTIONS; // each pointer will point to a vector
char **ENVIRONMENT;
int ENEMY[2];
int **ENEMIES;
double REWARD;
double REWARD_HISTORY[REWARD_HISTORY_SIZE];
double AVERAGE_REWARD;

double SENSORS[SENSORS_SIZE];
double STATE[STATE_SIZE];

// weight matrices for state transitions
double **Ws_entry;
double ***Ws_internal;
double **Ws_exit;

// neurons for state transition
double *Ns;
double *Ns_switch;
double *Ns_exit;

// weight matrices for the policy
double **Wp_entry; 
double ***Wp_internal;
double **Wp_exit;

double **Wpolicy_entry;
double ***Wpolicy_internal;
double **Wpolicy_exit;

//neurons for policy
double *Np;
double *Np_switch;
double *Np_exit;

char **GRID;

void update_sensors(void);
void update_state(void);
void update_action(void);
void update_environment(void);
void update_reward(void);
void update_grid(void);
void update_weight_matrices(void);
void update_enemies(void);
void update_physical_agent_state(void);

double read_sensors(int i );
double randfloat(double min, double max);

void weight_matrices_init();
void neurons_init();
void grid_init();
void environment_init();
void state_init();
void enemy_init();
void actions_init();
void enemies_init();



void print_grid();
void print_state();
void print_environment();





int find_optimal_action_index(void);
int find_actual_action_index(void);
void training_matrices_init();
void train_policy(void);

void compute_policy();

void compute_policy()
{
	double *Np_buf;

	for (int i = 0; i < POLICY_NEURONS; i++) {
		Np[i] = 0.0;
		for (int j = 0; j < STATE_SIZE; j++) {
			Np[i] += Wpolicy_entry[i][j] * STATE[j];
		}
		if (Np[i] > THRESHOLD)
			Np[i] = 1.0;
		else
			Np[i] = -1.0;
	}
	
	for (int i = 0; i < POLICY_LAYERS; i++) {
		for (int j = 0; j < POLICY_NEURONS; j++) {
			Np_switch[j] = 0.0;
			for (int k = 0; k < POLICY_NEURONS; k++) {
				Np_switch[j] += Wpolicy_internal[i][j][k] * Np[k];
			}
			if (Np_switch[j] > THRESHOLD)
				Np_switch[j] = 1.0;
			else
				Np_switch[j] = -1.0;
		}

			
		// simple optimization
		Np_buf = Np;
		Np = Np_switch;
		Np_switch = Np_buf;	
	}
	
	for (int i = 0; i < POLICY_EXIT_NEURONS; i++) {
		Np_exit[i] = 0.0;
		for (int j = 0; j < POLICY_NEURONS; j++) {
			Np_exit[i] += Wpolicy_exit[i][j] * Np[j];
		}
		if (Np_exit[i] > THRESHOLD)
			Np_exit[i] = 1.0;
		else
			Np_exit[i] = -1.0;
	}
	


	return;
}

int find_actual_action_index(void)
{
	// can optimize this out later by using global action index
	if (ACTION[0] > 0)
		return 1;
	if (ACTION[1] > 0)
		return 2;
	if (ACTION[0] < 0)
		return 3;
	if (ACTION[1] < 0)
		return 4;

	return 0;
}


void train_policy(void)
{
	double error_base;
	double error;
	int target_index = find_optimal_action_index();
	int actual_index = find_actual_action_index();
	double target_output[POLICY_EXIT_NEURONS];
	for (int i = 0; i < POLICY_EXIT_NEURONS; i++)
		target_output[i] = -1.0;
	for (int i = 0; i < NUM_ACTION_REPRESENTATIVES; i++)
		target_output[target_index*NUM_ACTION_REPRESENTATIVES + i] = 1.0;


	
		
	
	return;
}



int find_optimal_action_index(void)
{
	int dx, dy;
	
	dx = ENVIRONMENT_SIZE/2 - POSITION[0];
	dy = ENVIRONMENT_SIZE/2 - POSITION[1];
	
	if (dx > 0) 
		return 1;
	if (dy > 0)
		return 2;
	if (dx < 0)
		return 3;
	if (dy < 0)
		return 4;	

	return 0;
}







void print_environment()
{
	for (int i = 0; i < ENVIRONMENT_SIZE; i++) {
		for (int j = 0; j < ENVIRONMENT_SIZE; j++)
			printf(" %c ", ENVIRONMENT[i][j]);
		printf("\n");
	}

	return;
}

void print_state()
{
	double sum = 0.0;
	for (int i = 0; i < STATE_SIZE; i++)
		sum += STATE[i];
	printf("state sum: %f\n", sum);
	return;
}


void print_grid()
{
	for (int i = 0; i < RADIUS; i++) {
		for (int j = 0; j < RADIUS; j++) {
			if (GRID[i][j] == 'x') {
				printf(" x ");
			} else {
				printf(" . ");
			}
		}
		printf("\n");
	}

	return;
}


double read_sensors(int i)
{
	double sensor_state;
	// here we map i to a position on the grid
	int x, y;
	x = i % RADIUS;
	y = i / RADIUS;

	if (GRID[y][x] == 'x')
		sensor_state = 1.0;
	else
		sensor_state = -1.0;
	return sensor_state;
}

void update_grid(void)
{
	int dx, dy;
	int x, y;
	// if there is an enemy in the grid location, put an x in it
	
	// first clear the grid
	for (int i = 0; i < RADIUS; i++)
		for (int j = 0; j < RADIUS; j++)
			GRID[i][j] = '.';	

	for (int i = 0; i < ENVIRONMENT_SIZE; i++) {
		for (int j = 0; j < ENVIRONMENT_SIZE; j++) {
			if (ENVIRONMENT[i][j] == 'x') {
				dx = i - POSITION[0];
				//printf("dx: %i\n", dx);
				dx += RADIUS/2;
				dy = j - POSITION[1];
				//printf("dy: %i\n", dy);
			       	dy += RADIUS/2;
				if (dx >= 0 && dx < RADIUS && dy >= 0 && dy < RADIUS) {
					GRID[dx][dy] = 'x';
				}
			}
		}
	}	
	return;
}


void update_reward(void)
{
	int center_x = ENVIRONMENT_SIZE / 2;
	int center_y = ENVIRONMENT_SIZE / 2;
	int dx = POSITION[0] - center_x;
	int dy = POSITION[1] - center_y;
	double ddx = (double)dx;
	double ddy = (double)dy;
	double r = ddx*ddx + ddy*ddy;
	r = 1 / r;
	REWARD = MAX_REWARD;
	if (r > 0 && r < MAX_REWARD)
		REWARD = r;
	for (int i = REWARD_HISTORY_SIZE - 1; i > 0; i--)
		REWARD_HISTORY[i] = REWARD_HISTORY[i-1];
	REWARD_HISTORY[0] = REWARD;
	
	double avg = 0.0;
	for (int i = 0; i < REWARD_HISTORY_SIZE; i++)
		avg += REWARD_HISTORY[i];
	avg = avg / REWARD_HISTORY_SIZE;
	AVERAGE_REWARD = avg;

	return;
}

void update_action(void)
{
	double *Np_buf;

	for (int i = 0; i < POLICY_NEURONS; i++) {
		Np[i] = 0.0;
		for (int j = 0; j < STATE_SIZE; j++) {
			Np[i] += Wp_entry[i][j] * STATE[j];
		}
		if (Np[i] > THRESHOLD)
			Np[i] = 1.0;
		else
			Np[i] = -1.0;
	}
	
	for (int i = 0; i < POLICY_LAYERS; i++) {
		for (int j = 0; j < POLICY_NEURONS; j++) {
			Np_switch[j] = 0.0;
			for (int k = 0; k < POLICY_NEURONS; k++) {
				Np_switch[j] += Wp_internal[i][j][k] * Np[k];
			}
			if (Np_switch[j] > THRESHOLD)
				Np_switch[j] = 1.0;
			else
				Np_switch[j] = -1.0;
		}

			
		// simple optimization
		Np_buf = Np;
		Np = Np_switch;
		Np_switch = Np_buf;	
	}
	
	for (int i = 0; i < POLICY_EXIT_NEURONS; i++) {
		Np_exit[i] = 0.0;
		for (int j = 0; j < POLICY_NEURONS; j++) {
			Np_exit[i] += Wp_exit[i][j] * Np[j];
		}
		if (Np_exit[i] > THRESHOLD)
			Np_exit[i] = 1.0;
		else
			Np_exit[i] = -1.0;
	}
	
	double action_vote[NUM_ACTIONS];
	for (int i = 0; i < NUM_ACTIONS; i++) {
		action_vote[i] = 0.0;
		for (int j = 0; j < NUM_ACTION_REPRESENTATIVES; j++)
			action_vote[i] += Np_exit[NUM_ACTIONS*i + j];
	}

	int chosen_action_index = 0;
	double current_max = action_vote[0];
	for (int i = 1; i < NUM_ACTIONS; i++) {
		if (action_vote[i] > current_max) {
			chosen_action_index = i;
			current_max = action_vote[i];
		}
	}
	
	ACTION[0] = ACTIONS[chosen_action_index][0];
	ACTION[1] = ACTIONS[chosen_action_index][1];

	return;
}

void update_enemies(void)
{
	int r;
	for (int i = 0; i < NUM_ENEMIES; i++) {
		r = rand() % NUM_ACTIONS;
		ENEMIES[i][0] = (ENEMIES[i][0] + ACTIONS[r][0] + ENVIRONMENT_SIZE) % ENVIRONMENT_SIZE;
		ENEMIES[i][1] = (ENEMIES[i][1] + ACTIONS[r][1] + ENVIRONMENT_SIZE) % ENVIRONMENT_SIZE;
	}

	return;
}


void update_environment(void)
{
	int dx, dy;
	int r = rand() % NUM_ACTIONS;

	//ENEMY[0] = (ENEMY[0] + ACTIONS[r][0] + ENVIRONMENT_SIZE) % ENVIRONMENT_SIZE;
	//ENEMY[1] = (ENEMY[1] + ACTIONS[r][1] + ENVIRONMENT_SIZE) % ENVIRONMENT_SIZE;
	
	for (int i = 0; i < ENVIRONMENT_SIZE; i++)
		for (int j = 0; j < ENVIRONMENT_SIZE; j++)
			ENVIRONMENT[i][j] = '.';	

	for (int i = 0; i < NUM_ENEMIES; i++)
		ENVIRONMENT[ENEMIES[i][0]][ENEMIES[i][1]] = 'x';

	ENVIRONMENT[POSITION[0]][POSITION[1]] = '*';

	return;
}


void update_agent_physical_state(void)
{
	POSITION[0] = (POSITION[0] + ACTION[0] + ENVIRONMENT_SIZE) % ENVIRONMENT_SIZE;
	POSITION[1] = (POSITION[1] + ACTION[1] + ENVIRONMENT_SIZE) % ENVIRONMENT_SIZE;

	return;
}


void update_sensors(void)
{
	update_grid();
	for (int i = STATE_TRANSITION_EXIT_NEURONS; i < STATE_SIZE; i++) {
		if (i / RADIUS  < RADIUS)
			STATE[i] = read_sensors(i);
	}

	return;
}


void update_state(void)
{
	double *Ns_buf;

	for (int i = 0; i < STATE_TRANSITION_NEURONS; i++) {
		Ns[i] = 0.0;
		for (int j = 0; j < STATE_SIZE; j++) {
			Ns[i] += Ws_entry[i][j] * STATE[j];
		}
		if (Ns[i] > THRESHOLD)
			Ns[i] = 1.0;
		else
			Ns[i] = -1.0;
	}

	for (int i = 0; i < STATE_TRANSITION_LAYERS; i++) {
		for (int j = 0; j < STATE_TRANSITION_NEURONS; j++) {
			Ns_switch[j] = 0.0;
			for (int k = 0; k < STATE_TRANSITION_NEURONS; k++) {
				Ns_switch[j] += Ws_internal[i][j][k] * Ns[k];
			}
			if (Ns_switch[j] > THRESHOLD)
				Ns_switch[j] = 1.0;
			else
				Ns_switch[j] = -1.0;
		}

			
		// simple optimization
		Ns_buf = Ns;
		Ns = Ns_switch;
		Ns_switch = Ns_buf;	
	}

	for (int i = 0; i < STATE_TRANSITION_EXIT_NEURONS; i++) {
		STATE[i] = 0.0;
		for (int j = 0; j < STATE_TRANSITION_NEURONS; j++) {
			STATE[i] += Ws_exit[i][j] * Ns[j];
		}
		if (STATE[i] > THRESHOLD)
			STATE[i] = 1.0;
		else
			STATE[i] = -1.0;
	}

	return;
}


void enemies_init()
{
	ENEMIES = (int **)malloc(NUM_ENEMIES * sizeof(int *));
	for (int i = 0; i < NUM_ENEMIES; i++) {
		ENEMIES[i] = (int *)malloc(2 * sizeof(int));
		// paranoid... making sure this is positive (too lazy to write unsigned)
		ENEMIES[i][0] = rand() % ENVIRONMENT_SIZE;
		ENEMIES[i][0] += ENVIRONMENT_SIZE;
		ENEMIES[i][0] = ENEMIES[i][0] % ENVIRONMENT_SIZE;

		ENEMIES[i][1] = rand() % ENVIRONMENT_SIZE;
		ENEMIES[i][1] += ENVIRONMENT_SIZE;
		ENEMIES[i][1] = ENEMIES[i][1] % ENVIRONMENT_SIZE;
	}

	return;
}


void grid_init()
{
	GRID = (char **)malloc(RADIUS * sizeof(char *));
	for (int r = 0; r < RADIUS; r++)
		GRID[r] = (char *)malloc(RADIUS * sizeof(char));


	return;
}

void actions_init()
{
	ACTIONS = (int **)malloc(NUM_ACTIONS * sizeof(int *));
	for (int i = 0; i < NUM_ACTIONS; i++) {
		ACTIONS[i] = (int *)malloc(2*sizeof(int));
	}

	ACTIONS[0][0] = 0;
	ACTIONS[0][1] = 0;
	ACTIONS[1][0] = 1;
	ACTIONS[1][1] = 0;
	ACTIONS[2][0] = 0;
	ACTIONS[2][1] = 1;
	ACTIONS[3][0] = -1;
	ACTIONS[3][1] = 0;
	ACTIONS[4][0] = 0;
	ACTIONS[4][1] = -1;
	
	return;
}

void environment_init()
{
	ENVIRONMENT = (char **)malloc(ENVIRONMENT_SIZE * sizeof(char *));
	for (int i = 0; i < ENVIRONMENT_SIZE; i++) {
		ENVIRONMENT[i] = (char *)malloc(ENVIRONMENT_SIZE * sizeof(char));
		for (int j = 0; j < ENVIRONMENT_SIZE; j++)
			ENVIRONMENT[i][j] = '.';
	}	
	
	for (int i = 0; i < NUM_ENEMIES; i++)
		ENVIRONMENT[ENEMIES[i][0]][ENEMIES[i][1]] = 'x';

	ENVIRONMENT[POSITION[0]][POSITION[1]] = '*';
	
	return;
}

void neurons_init()
{
	Ns = (double *)malloc(STATE_TRANSITION_NEURONS * sizeof(double));
	Ns_switch = (double *)malloc(STATE_TRANSITION_NEURONS * sizeof(double));

	Np = (double *)malloc(POLICY_NEURONS * sizeof(double));
	Np_switch = (double *)malloc(POLICY_NEURONS * sizeof(double));
	Np_exit = (double *)malloc(POLICY_EXIT_NEURONS * sizeof(double));
}


void weight_matrices_init()
{
	Ws_entry = (double **)malloc(STATE_TRANSITION_NEURONS * sizeof(double *));
	Ws_internal = (double ***)malloc(STATE_TRANSITION_LAYERS * sizeof(double **));
	Ws_exit = (double **)malloc(STATE_SIZE * sizeof(double *));

	Wp_entry = (double **)malloc(STATE_SIZE * sizeof(double *));
	Wp_internal = (double ***)malloc(POLICY_LAYERS * sizeof(double **));
	Wp_exit = (double **)malloc(NUM_ACTIONS * sizeof(double *));
	Wpolicy_entry = (double **)malloc(STATE_SIZE * sizeof(double *));
	Wpolicy_internal = (double ***)malloc(POLICY_LAYERS * sizeof(double **));
	Wpolicy_exit = (double **)malloc(NUM_ACTIONS * sizeof(double *));
	
	for (int i = 0; i < STATE_TRANSITION_NEURONS; i++) {
		Ws_entry[i] = (double *)malloc(STATE_SIZE * sizeof(double)); // allocating row that is used in the product with the state column vector
		for (int j = 0; j < STATE_SIZE; j++) {
			Ws_entry[i][j] = randfloat(-1.0, 1.0); 
		}
	}

	for (int i = 0; i < STATE_TRANSITION_LAYERS; i++) {
		Ws_internal[i] = (double **)malloc(STATE_TRANSITION_NEURONS * sizeof(double *)); // allocating array where rows will be referenced
		for (int j = 0; j < STATE_TRANSITION_NEURONS; j++) {
			Ws_internal[i][j] = (double *)malloc(STATE_TRANSITION_NEURONS * sizeof(double)); // allocating row vector in matrix
			for (int k = 0; k < STATE_TRANSITION_NEURONS; k++) {
				Ws_internal[i][j][k] = randfloat(-1.0, 1.0);
			}
		}
	}

	for (int i = 0; i < STATE_TRANSITION_EXIT_NEURONS; i++) {
		Ws_exit[i] = (double *)malloc(STATE_TRANSITION_NEURONS * sizeof(double)); // row vector for exit layer (must be size of state)
		for (int j = 0; j < STATE_TRANSITION_NEURONS; j++) {
			Ws_exit[i][j] = randfloat(-1.0, 1.0);	
		}
	}

	for (int i = 0; i < POLICY_NEURONS; i++) {
		Wp_entry[i] = (double *)malloc(STATE_SIZE * sizeof(double));
		for (int j = 0; j < STATE_SIZE; j++) {
			Wp_entry[i][j] = randfloat(-1.0, 1.0);
		}
	}

	for (int i = 0; i < POLICY_LAYERS; i++) {
		Wp_internal[i] = (double **)malloc(POLICY_NEURONS * sizeof(double *));
		for (int j = 0; j < POLICY_NEURONS; j++) {
			Wp_internal[i][j] = (double *)malloc(POLICY_NEURONS * sizeof(double));
			for (int k = 0; k < POLICY_NEURONS; k++) {
				Wp_internal[i][j][k] = randfloat(-1.0, 1.0);
			}
		}
	}

	for (int i = 0; i < POLICY_EXIT_NEURONS; i++) {
		Wp_exit[i] = (double *)malloc(POLICY_NEURONS * sizeof(double));
		for (int j = 0; j < POLICY_NEURONS; j++) {
			Wp_exit[i][j] = randfloat(-1.0, 1.0);
		}
	}


	for (int i = 0; i < POLICY_NEURONS; i++) {
		Wpolicy_entry[i] = (double *)malloc(STATE_SIZE * sizeof(double));
		for (int j = 0; j < STATE_SIZE; j++) {
			Wpolicy_entry[i][j] = randfloat(-1.0, 1.0);
		}
	}

	for (int i = 0; i < POLICY_LAYERS; i++) {
		Wpolicy_internal[i] = (double **)malloc(POLICY_NEURONS * sizeof(double *));
		for (int j = 0; j < POLICY_NEURONS; j++) {
			Wpolicy_internal[i][j] = (double *)malloc(POLICY_NEURONS * sizeof(double));
			for (int k = 0; k < POLICY_NEURONS; k++) {
				Wpolicy_internal[i][j][k] = randfloat(-1.0, 1.0);
			}
		}
	}

	for (int i = 0; i < POLICY_EXIT_NEURONS; i++) {
		Wpolicy_exit[i] = (double *)malloc(POLICY_NEURONS * sizeof(double));
		for (int j = 0; j < POLICY_NEURONS; j++) {
			Wpolicy_exit[i][j] = randfloat(-1.0, 1.0);
		}
	}



	return;
}

void state_init()
{
	for (int i = 0; i < STATE_SIZE; i++)
		STATE[i] = randfloat(-1.0, 1.0);

	return;
}

double randfloat(double min, double max)
{
	double range = max - min;
	double div = RAND_MAX / range;
	return min + (rand() / div);
}

int main(void)
{
	srand(time(NULL));

	POSITION[0] = rand() % ENVIRONMENT_SIZE;
	POSITION[1] = rand() % ENVIRONMENT_SIZE;

	state_init();
	weight_matrices_init();
	neurons_init();
	actions_init();
	grid_init();
	enemies_init();
	environment_init();

	int t = 0;
	for (;;) {
		printf("time: %i\n", t);
		printf("position: %i, %i\n", POSITION[0], POSITION[1]);
		t += 1;
		update_sensors();
		update_state();
		update_action();
		update_agent_physical_state();
		update_enemies();
		update_environment();
		update_reward();
		//print_environment();
		print_grid();
		//printf("reward: %f\n", REWARD);
		//printf("average reward: %f\n", AVERAGE_REWARD);
	}
}
